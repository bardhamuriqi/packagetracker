/*
 The function below is called when the search button is clicked,
 it makes a request to the backend and returns estimated delivery date.
*/
$('.search').click(function(event) {

    // Checks if the input field is empty
    if($('.displayed input').val()==""){
        event.stopPropagation();
        event.preventDefault();
        $('.displayed input').css({
            'border-color':'red',
            'transition':'all 0.3s'
        })
    }
    else{

        var trackingCode = $('.displayed input').val();
        // Makes the request
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/tracking",
            data: {trackingCode:trackingCode},
        complete: function(result){
            // Returns estimated delivery date
            $('.hidden-result p').text(result.responseText);
                $('.displayed input').css('border-color','#ccc')
                $('.displayed').css({
                'visibility':'hidden',
                'opacity':'0',
                'position':'absolute',
                'transition':'all 0s'
                })
                $('.hidden-result').css({
                'visibility':'visible',
                'opacity':'1',
                'position':'relative',
                'transition':'all 0.3s'
                })}
        });
    }
})

// Resets the form
$('.reset').click(function() {
    $('.displayed input').val('')
    $('.displayed').css({
        'visibility':'visible',
        'opacity':'1',
        'position':'relative',
        'transition':'all 0.3s'
    })
    $('.hidden-result').css({
        'visibility':'hidden',
        'opacity':'0',
        'position':'absolute',
        'transition':'all 0s'
    })
})