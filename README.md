# Package Tracking System 

## Introduction

The project is build on Laravel as a Backend and jQuery library as frontend using Ajax to make a request to the backend, and pull the estimated devlivery.

## Usage

Add your database credentials to the necessary env fields

Add the absolute path of your CSV file for the DATA_SOURCE field

Run the database migrations 
   php artisan migrate

Run the database seeders
   php artisan db:seed

Run the application
   php artisan serve
