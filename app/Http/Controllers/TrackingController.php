<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tracker;

class TrackingController extends Controller
{
    /**
     * Returns estimated delivery date of the 
     * package tracking code.
     *
     * @return string $deliveryDate
     */
    public function getDeliveryDate(Request $request)
    {
        $trackingCode = $request->trackingCode;

        $tracker = Tracker::where('tracking_code', $trackingCode)->first();
        if( $tracker == null) {
            return "Package with this tracking code not found!";
        }
        
        $deliveryDate = $tracker->delivery_date;
        return $deliveryDate;
    }

    /**
     * Reads array and inserts it in our database.
     *
     * @return string $deliveryDate
     */
    public function importCsv()
    {
        $file = env("DATA_SOURCE");
        $packages = $this->csvToArray($file);

        foreach($packages as $package)
        {
            Tracker::firstOrCreate($package);
        }

        return 'Data insert successfully!';    
    }

    /**
     * Reads the CSV file and converts it to an array.
     *
     * @return array $data
     */
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

}
