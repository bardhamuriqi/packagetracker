<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Tracking System</title>

	<!-- CSS File Include for the app and External Bootstrap libraries -->
	<link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<div class="content">
			<div class="displayed">
				<div class="input">
					<h4>Enter your tracking code</h4>
					<input type="text" class="form-control" name="tracking-code">
				</div>
				<button class="btn btn-info search" name="search">SEARCH</button>
			</div>

			<div class="hidden-result">
				<h4>Estimated delivery date:</h4>
				<p></p>
				<div class="reset-btn">
					<button class="btn btn-info reset" name="reset">RESET</button>	
				</div>
			</div>
		</div>
	</div>	
</body>
<footer>
	<!-- JS File Include for the app -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<script src="{{ asset('js/script.js') }}" ></script>
</footer>
</html>