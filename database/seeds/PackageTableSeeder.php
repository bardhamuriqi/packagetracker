<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Tracker;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = [
            ['tracking_code' => Str::random(10), 'delivery_date' => Carbon::create('2019', '05', '20')],
            ['tracking_code' => Str::random(10), 'delivery_date' => Carbon::create('2019', '06', '20')],
            ['tracking_code' => Str::random(10), 'delivery_date' => Carbon::create('2019', '05', '06')],
        ];

        foreach($packages as $package){
            Tracker::create($package);
        }
    }
}
