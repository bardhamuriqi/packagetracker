<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrackerTest extends TestCase
{
    /**
     * Testing for method getDeliveryDate.
     *
     * @return void
     */
    public function testGetDeliveryDate()
    {
        $data = [
            'tracking_code' => '4eXS0oS8NW'
        ];

        $response = $this->json('POST', '/tracking', $data);
        $response->assertStatus(200);
       
    }

    /**
     * Testing for method importCsv.
     *
     * @return void
     */
    public function testImportCsv()
    {
        $response = $this->json('GET', '/data');
        $response->assertStatus(200);
    }

}
